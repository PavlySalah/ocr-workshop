'''
Calculate WER and CER
Given a reference (ground truth) file and
a hypothesis (algorithm-generated) file
'''
import numpy as np

def split_file(path):
    '''
    Given file path, split words

    Args:
        path (str): file path

    Returns:
        text_split (list of strings): list of words in file
    '''
    # Read text from file
    with open(path, 'r') as f:
        text = f.readlines()

    # Split text into words and ignore extra spaces
    split_text = [x.strip() for t in text for x in t.split(' ') if len(x) > 1]

    return split_text


def split_file_IER(path):
    '''
    Given file path, split words

    Args:
        path (str): file path

    Returns:
        text_split (list of strings): list of words in file
    '''
    # Read text from file
    with open(path, 'r') as f:
        text = f.readlines()

    # Split text into words and ignore extra spaces
    split_text = [x.strip() for t in text for x in t.split('\n') if len(x) > 1]

    return split_text


def calculate_cer(word1, word2):
    '''
    Calculate the CER between two words

    Sauce: Levenshtein distance

    Args:
        word1 (str): first word to compare
        word2 (str): second word to compare

    Returns:
        cer_int (int): number of different characters
                       between the two words
        cer_pct (float): ratio of different characters
                       to total number of characters
    '''
    # Initialize CER matrix
    total_chars = max(len(word1), len(word2))
    size_x = len(word1) + 1
    size_y = len(word2) + 1

    matrix = np.zeros((size_x, size_y))

    for x in range(size_x):
        matrix [x, 0] = x

    for y in range(size_y):
        matrix [0, y] = y

    # Calculate CER matrix
    for x in range(1, size_x):
        for y in range(1, size_y):
            if word1[x-1] == word2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )

    # Total CER
    cer = matrix[size_x-1, size_y-1]

    # CER percentage
    cer_pct = cer/total_chars

    return cer, cer_pct


def calc_cer_file(path1, path2):
    '''
    Calculate CER between two files

    Args:
        path1 (str): path of the reference file
        path2 (str): path of the hypothesis file

    Returns:
        cers_int (int): total CER
        cers_pct (float): average CER accross the files
    '''
    # Split files
    file1_text = split_file(path1)
    file2_text = split_file(path2)

    cers_pct = []
    cers_int = 0

    # Calculate CER across files
    for w1, w2 in zip(file1_text, file2_text):
        # print('[DEBUG]', w1.strip(), ' => ', w2.strip())
        cer_int, cer_pct = calculate_cer(w1, w2)
        # print('[DEBUG] CER:', cer_int)

        # Save the CER results
        cers_int += cer_int
        cers_pct.append(cer_pct)

    # Average CER per document
    avg_cer = sum(cers_pct)/len(cers_pct)
    return (cers_int, avg_cer)


def clean_text(text):
    '''
    Clean text

    Args:
        text (array of strings): the list of words

    Returns:
        text_clean (array of strings): the list of clean words
    '''
    # Ignore case sensitivity
    text_clean = [re.lower() for re in text]

    return text_clean


def check_comp(word):
    '''
    Check if WER should be calculated between two words

    Args:
        word (str): word

    Returns:
        True or False: whether or not to calculate WER
    '''
    # Ignore useless characters
    useless_chars = ['(', ')', '|', '[', ']', '{', '}', '\'', ':', ' ']

    # If the word is just an irrelevant character
    # Or its length is 1 and it's not a digit
    if word in useless_chars or (len(word) < 2 and not word.isdigit()):
        return True

    return False


def calculate_distance_matrix(r, h):
    '''
    This function is to calculate the edit distance of
    reference sentence and the hypothesis sentence.

    Args:
        r (array of strings): the list of words produced by
                              splitting reference sentence.
        h (array of strings): the list of words produced by
                              splitting hypothesis sentence.

    Returns:
        d (array): distance matrix
    '''

    # Clean text
    r = [re.lower() for re in r]
    h = [he.lower() for he in h]

    # Initialize matrix
    d = np.zeros((len(r)+1)*(len(h)+1), dtype=np.uint8).reshape((len(r)+1, len(h)+1))

    for i in range(len(r)+1):
        d[i][0] = i

    for j in range(len(h)+1):
        d[0][j] = j

    # Claculate "distance" between the two arrays
    for i in range(1, len(r)+1):
        for j in range(1, len(h)+1):
            # print(f'[DEBUG] {r[i-1]} => {h[j-1]}')
            # Only count WER if the CER > 0.2
            # and check_comp return False
            if (calculate_cer(r[i-1], h[j-1])[1] < 0.3) or \
                (check_comp(r[i-1]) and check_comp(h[j-1])):
                d[i][j] = d[i-1][j-1]

            # Otherwise, compute WER
            else:
                substitute = d[i-1][j-1] + 1
                insert = d[i][j-1] + 1
                delete = d[i-1][j] + 1
                d[i][j] = min(substitute, insert, delete)
    return d


def calculate_wer(r, h):
    """
    Calculate the Word Error Rate

    Args:
        r (array of strings): the list of words produced by
                              splitting reference sentence.
        h (array of strings): the list of words produced by
                              splitting hypothesis sentence.

    Returns:
        wer (float): WER
    """
    # Build distance matrix
    d = calculate_distance_matrix(r, h)

    # Average WER per document (list of strings)
    max_len = len(r) if len(r) >= len(h) else len(h)
    wer = float(d[len(r)][len(h)]) / max_len
    return wer


def calc_wer_file(path1, path2):
    '''
    Calculate WER between two files

    Args:
        path1 (str): path of the reference file
        path2 (str): path of the hypothesis file

    Returns:
        wer (float): WER between the two files
    '''
    # Split files
    file1_text = split_file(path1)
    file2_text = split_file(path2)

    # Calculate WER
    wer = calculate_wer(file1_text, file2_text)

    return wer


def calculate_IER(path1, path2):
    '''
    IER = sum(WER(item))/total_num_items
    '''

    # Split files
    r = split_file(path1)
    h = split_file(path2)

    # Split files by items
    r_item = split_file_IER(path1)
    h_item = split_file_IER(path2)

    # Build distance matrix
    d = calculate_distance_matrix(r, h)

    # Average WER per document (list of strings)
    max_len = len(r_item) if len(r_item) >= len(h_item) else len(h_item)
    wer = float(d[len(r)][len(h)]) / max_len
    return wer


def main():
    '''main'''

    # Define the paths of the files
    path1 = './WER/f1.txt'
    path2 = './WER/f2.txt'

    # Caclulate the WER etween the two files
    wer = calc_wer_file(path1, path2)
    print(f'WER between {path1} and {path2}: {wer}')

    # Caclulate the WER etween the two files
    wer = calculate_IER(path1, path2)
    print(f'WER between {path1} and {path2}: {wer}')


if __name__ == '__main__':
    main()
