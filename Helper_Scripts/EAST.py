from imutils.object_detection import non_max_suppression
import cv2
import numpy as np
import pytesseract


class TextDetector():
	def __init__(self, east_path, width=320, height=320,
              min_confidence=0.5, padding=0.05, language="eng", oem=1, psm=7,
              rec=False, *args, **kwargs):
            '''
            Initialise text detector class.

            Args:
                east_path (str): path to EAST model
                width (int): image width
                height (int): image height
                min_confidence (float): minimum confidence to consider as a box
                padding (float): padding to a box
                rec (boolean): Whether or not to use recognition model
                language (str): language of Tesseract
                oem (int): OEM for Tesseract
                psm (int): PSM for Tesseract
            '''
		super().__init__(*args, **kwargs)
		self.east_path = east_path
		self.width = width
		self.height = height
		self.padding = padding
		self.min_confidence = min_confidence
		self.config = (f"-l {language} --oem {oem} --psm {psm}")
		self.rec=rec

	
	def __decode_predictions(self, scores, geometry):
		'''Private method: Return the decodes results and confidence values'''
		(numRows, numCols) = scores.shape[2:4]
		rects = []
		confidences = []

		for y in range(0, numRows):
			scoresData = scores[0, 0, y]
			xData0 = geometry[0, 0, y]
			xData1 = geometry[0, 1, y]
			xData2 = geometry[0, 2, y]
			xData3 = geometry[0, 3, y]
			anglesData = geometry[0, 4, y]

			for x in range(0, numCols):
				if scoresData[x] < self.min_confidence:
					continue

				(offsetX, offsetY) = (x * 4.0, y * 4.0)

				angle = anglesData[x]
				cos = np.cos(angle)
				sin = np.sin(angle)

				h = xData0[x] + xData2[x]
				w = xData1[x] + xData3[x]

				endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
				endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
				startX = int(endX - w)
				startY = int(endY - h)

				rects.append((startX, startY, endX, endY))
				confidences.append(scoresData[x])

		return (rects, confidences)


	def detect_text(self, image):
		'''
                Detect text in an image.
                Recognise text if self.rec is set to True

                Args:
                    image (numpy array): input image

                Returns:
                    output (numpy array): output image
                    text_array (list of str): list of output text
                '''
		print("[INFO] Detecting text")
		orig = image.copy()
		(origH, origW) = image.shape[:2]


		(newW, newH) = (self.width, self.height)
		rW = origW / float(newW)
		rH = origH / float(newH)

		image = cv2.resize(image, (newW, newH))
		(H, W) = image.shape[:2]

		layerNames = [
			"feature_fusion/Conv_7/Sigmoid",
			"feature_fusion/concat_3"]

		print("[INFO] loading EAST text detector...")
		net = cv2.dnn.readNet(self.east_path)

		blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
			(123.68, 116.78, 103.94), swapRB=True, crop=False)
		net.setInput(blob)
		(scores, geometry) = net.forward(layerNames)

		(rects, confidences) = self.__decode_predictions(scores, geometry)
		boxes = non_max_suppression(np.array(rects), probs=confidences)

		results = []

		text_array = []

		for (startX, startY, endX, endY) in boxes:
			startX = int(startX * rW)
			startY = int(startY * rH)
			endX = int(endX * rW)
			endY = int(endY * rH)

			dX = int((endX - startX) * self.padding)
			dY = int((endY - startY) * self.padding)

			startX = max(0, startX - dX)
			startY = max(0, startY - dY)
			endX = min(origW, endX + (dX * 2))
			endY = min(origH, endY + (dY * 2))

			roi = orig[startY:endY, startX:endX]

			text = ''
			if self.rec:
				text = pytesseract.image_to_string(roi, config=self.config)

			results.append(((startX, startY, endX, endY), text))

		output = orig.copy()

		for ((startX, startY, endX, endY), text) in results:
			cv2.rectangle(output, (startX, startY), (endX, endY),
				(200, 155, 100), 2)

			if self.rec:
				cv2.putText(output, text.strip(), (startX, startY - 20),
					cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

		return output, text_array
